Part 3
1.) Needs: [] will make a job run out of order meaning that it will run when the jobs that it depends on to run finish. 
2.) The when instruction will tell when the job should run. It is defaulted to when:on_success. 
3.) You would use rules to restrict by calling an if statement to check the branch_name to see if it equals verified-.
4.) Yes it is possible to override CI default variables.
5.) If you use multiple caches to combine with a fallback cache key, the fallback cache key would be fetched everytime a cache is not found. 
