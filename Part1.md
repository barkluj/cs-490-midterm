Part 1
1.) Commit will save all changes, alongside a description of what was changed in the local repository.
2.) They can all be staged and committed.
3.) Creating a new branch should be done when the work is experimental.
4.) Conflicts will happen when two branches have made changes to a file and one wants to delete the file and one wants to modify the same file.
5.) To commit one branch specifically you would use git cherry-pick COMMIT_ID
