Part 2
1.) The reason to use containers is for ease of deployment, patching, and scalability.
2.) Docker compose files allow for hosting of multiple isolated environments onto one host.
3.) There can only be one ENTRYPOINT line in a dockerfile.
4.) Combining ENTRYPOINT and CMD should be done when you need a container with a certain executable, and also needs a default parameter, that can be modified.
5.) You should not use the latest tag, however, it would only be used if you declare nothing else meaning it is the default tag.
